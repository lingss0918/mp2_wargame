import java.io.IOException;


public class Game {
	int count=0;
	public boolean drop(Board b,int i, int j, String turn){
		if(i>1){ 
			if(b.board[i-1][j].occupied==turn ) return false;
		}
		if (i<5){
			if(b.board[i+1][j].occupied==turn) return false;
		}
		if(j>1){ 
			if(b.board[i][j-1].occupied==turn ) return false;
		}
		if (j<5){
			if(b.board[i][j+1].occupied==turn) return false;
		}
		
		return true;
	}
	
	public void blitz(Board b,int i, int j, String turn, String other_turn){
		
		if(i>1){ 
			if(b.board[i-1][j].occupied==other_turn ) b.board[i-1][j].occupied=turn;
		}
		if (i<5){
			if(b.board[i+1][j].occupied==other_turn) b.board[i+1][j].occupied=turn;
		}
		if(j>1){ 
			if(b.board[i][j-1].occupied==other_turn ) b.board[i][j-1].occupied=turn;
		}
		if (j<5){
			if(b.board[i][j+1].occupied==other_turn ) b.board[i][j+1].occupied=turn;
		}
		
		
	}
	public Board alpha_beta(Board b,int depth,String turn){
		count=0;
		int remain=b.remain();
		if(depth>remain) depth=remain;
		Board ret=null;
		int end=0;
		String other_turn="green";
		if (turn=="green") other_turn="blue";
		int max=Integer.MIN_VALUE;
		for(int i=0;i<6;i++){
			for (int j=0;j<6;j++){
				if(b.board[i][j].occupied=="unoccupied"){
					count++;
					if(drop(b,i,j,turn)) {
						int value=min_value(new Board(b,i,j,turn),depth-1,other_turn,turn,Integer.MIN_VALUE,Integer.MAX_VALUE);
						if (value>max){
							max=value;
							ret=new Board(b,i,j,turn);
							System.out.println("drop"+i+" "+j+" "+value+" "+turn);
						}
						end=1;
						
					}
					else{
						int value=min_value(new Board(b,i,j,turn,other_turn),depth-1,other_turn,turn,Integer.MIN_VALUE,Integer.MAX_VALUE);
						if (value>max){
							max=value;
							ret=new Board(b,i,j,turn,other_turn);
							System.out.println("bltz"+i+" "+j+" "+value+turn);
						}
						end=1;
					}
				}
			}
		}
		if(end==0) return b;
		System.out.println("count: "+count);
		return ret;
		
		
		
		
	}
	
	
	public int max_value(Board b,int depth,String turn,String t,int alpha,int beta){
		String other_turn="green";
		if (turn=="green") other_turn="blue";
		
		if (depth==0) return evaluation(b,t);
		int ret;
		int v=min_value(new Board(b,turn),depth-1,other_turn,t,alpha,beta);
		for(int i=0;i<6;i++){
			for (int j=0;j<6;j++){
				if(b.board[i][j].occupied=="unoccupied"){
					count++;
					if(drop(b,i,j,turn)) ret=min_value(new Board(b,i,j,turn),depth-1,other_turn,t,alpha,beta);
					else{
						ret=min_value(new Board(b,i,j,turn,other_turn),depth-1,other_turn,t,alpha,beta);
					}
					v=Math.max(v, ret);
					if (v>=beta) return v;
					alpha=Math.max(alpha,v);
				}
			}
		}

		return v;
		
	}
	
	public int min_value(Board b,int depth,String turn,String t,int alpha,int beta){
		String other_turn="green";
		if (turn=="green") other_turn="blue";
		
		if (depth==0) return evaluation(b,t);
		int ret;
		int v=max_value(new Board(b,turn),depth-1,other_turn,t,alpha,beta);
		for(int i=0;i<6;i++){
			for (int j=0;j<6;j++){
				if(b.board[i][j].occupied=="unoccupied"){
					count++;
					if(drop(b,i,j,turn)) ret=max_value(new Board(b,i,j,turn),depth-1,other_turn,t,alpha,beta);
					else{
						ret=max_value(new Board(b,i,j,turn,other_turn),depth-1,other_turn,t,alpha,beta);
					}
					v=Math.min(v, ret);
					if (v<=alpha) return v;
					beta=Math.min(beta,v);
				}
			}
		}

		return v;
		
	}
	
	
	
	public Board minimax(Board b,int depth,String turn){
		count=0;
		int remain=b.remain();
		if(depth>remain) depth=remain;
		Board ret=null;
		int end=0;
		String other_turn="green";
		if (turn=="green") other_turn="blue";
		int max=Integer.MIN_VALUE;
		for(int i=0;i<6;i++){
			for (int j=0;j<6;j++){
				if(b.board[i][j].occupied=="unoccupied"){
					count++;
					if(drop(b,i,j,turn)) {
						int value=minimax_rec(new Board(b,i,j,turn),depth-1,other_turn,turn);
						if (value>max){
							max=value;
							ret=new Board(b,i,j,turn);
							System.out.println("drop"+i+" "+j+" "+value+" "+turn);
						}
						end=1;
						
					}
					else{
						int value=minimax_rec(new Board(b,i,j,turn,other_turn),depth-1,other_turn,turn);
						if (value>max){
							max=value;
							ret=new Board(b,i,j,turn,other_turn);
							System.out.println("bltz"+i+" "+j+" "+value+turn);
						}
						end=1;
					}
				}
			}
		}
		if(end==0) return b;
		System.out.println("count: "+count);
		return ret;
			
	}
	
	
	public int minimax_rec(Board b,int depth,String turn,String t){
		String other_turn="green";
		if (turn=="green") other_turn="blue";
		int ret;
		if (depth==0) return evaluation(b,t);
		if(t!=turn) {
			ret=Integer.MAX_VALUE;
			for(int i=0;i<6;i++){
				for (int j=0;j<6;j++){
					if(b.board[i][j].occupied=="unoccupied"){
						count++;
						if(drop(b,i,j,turn)) ret=Math.min(ret,minimax_rec(new Board(b,i,j,turn),depth-1,other_turn,t));
						else{
							ret=Math.min(ret,minimax_rec(new Board(b,i,j,turn,other_turn),depth-1,other_turn,t));
						}
					}
				}
			}
		}
		else {
			ret=Integer.MIN_VALUE;
			for(int i=0;i<6;i++){
				for (int j=0;j<6;j++){
					if(b.board[i][j].occupied=="unoccupied"){
						count++;
						if(drop(b,i,j,turn)) ret=Math.max(ret,minimax_rec(new Board(b,i,j,turn),depth-1,other_turn,t));
						else{
							int val=minimax_rec(new Board(b,i,j,turn,other_turn),depth-1,other_turn,t);
							ret=Math.max(ret,val);
						}
					}
				}
			}
		}
		return ret;
		
	}
	
	
	public boolean end(Board b){
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				if (b.board[i][j].occupied=="unoccupied")
					return false;
				
			}
		}
		return true;
	}
	
	public int evaluation(Board b,String turn){
		int sum=0;;
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				if (b.board[i][j].occupied==turn)
				sum+=b.board[i][j].value;
				
			}
		}
		
		return sum;
		
	}
	
	
	public static void main(String[] args) throws NumberFormatException, IOException{
		Game g=new Game();
		Board b=new Board("data/Smolensk.txt");
		String turn="green";
		int count=0;
		while(!g.end(b)){
			System.out.println(++count);
			b=g.alpha_beta(b,3,turn);
			if (turn=="green") turn="blue";
			else turn="green";
			b.printboard();
		}
		System.out.println("green score: "+g.evaluation(b, "green"));
		System.out.println("blue score: "+g.evaluation(b, "blue"));
	}
}
