import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;



public class Board {
	public Piece[][] board;
	
	public Board (String filePath) throws NumberFormatException, IOException{
		board=new Piece[6][6];

		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		String line = null;
		int i=0;
		while ((line = reader.readLine()) != null) {
			String[] tokens = line.split("\t");
			
			for(int j=0;j<6;j++){
				//System.out.print(tokens[j]);
				board[i][j]=new Piece();
				board[i][j].value=Integer.parseInt(tokens[j]);
			}	
			i++;
					
		}
			
		
	}
	
	
	public Board(Board b, int x, int y, String turn) {
		board=new Piece[6][6];
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				board[i][j]=new Piece();
				board[i][j].value=b.board[i][j].value;
				board[i][j].occupied=b.board[i][j].occupied;
			}
		}	
		board[x][y].occupied=turn;
	}

	public Board(Board b, int x, int y, String turn, String other_turn) {
		board=new Piece[6][6];
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				board[i][j]=new Piece();
				board[i][j].value=b.board[i][j].value;
				board[i][j].occupied=b.board[i][j].occupied;
			}
		}
		if(x>1){ 
			if(board[x-1][y].occupied==other_turn ) board[x-1][y].occupied=turn;
		}
		if (x<5){
			if(board[x+1][y].occupied==other_turn) board[x+1][y].occupied=turn;
		}
		if(y>1){ 
			if(board[x][y-1].occupied==other_turn ) board[x][y-1].occupied=turn;
		}
		if (y<5){
			if(board[x][y+1].occupied==other_turn ) board[x][y+1].occupied=turn;
		}
		board[x][y].occupied=turn;
	}

	public void printboard(){
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				System.out.print(board[i][j].value+board[i][j].occupied+"  ");
			}
			System.out.print("\n");
		}	
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException{
		Board b=new Board("data/keren.txt");
		b.printboard();
	}

	public int remain(){
		int ret=0;
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){

				if(board[i][j].occupied=="unoccupied")
					ret++;
			}
		}	
		return ret;
	}

	
	public Board(Board b, String turn) {
		board=new Piece[6][6];
		int max=0;
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				board[i][j]=new Piece();
				board[i][j].value=b.board[i][j].value;
				board[i][j].occupied=b.board[i][j].occupied;
				if (board[i][j].occupied=="unoccupied") max=Math.max(max, board[i][j].value);
			}
		}
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				
				if (board[i][j].value==max) {
					board[i][j].occupied=turn;
					return;
				}
			}
		}
		
	} 
	
	
	
	
}
